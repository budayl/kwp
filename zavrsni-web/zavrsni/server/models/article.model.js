const mongoose = require("mongoose");
const objectId = mongoose.Schema.Types.ObjectId;

const articleSchema = new mongoose.Schema({
    title:{
        type: String,
        required: true
    },
    desc:{
        type: String
    },
    content: {
        type: [mongoose.Schema.Types.Mixed]
    },
    uploadDate: { 
        type: Date, 
        required: true 
    },
    owner: { 
        id:{
            type: objectId, 
            ref: "User", 
            required: true
        },
        username:{
            type: String,
            required: true
        }
    },
    comments:{
        type: [mongoose.Schema.Types.Mixed]
    }
}, { strict: false });

articleSchema.methods.setTitle = function(title) {
    this.content = content;
}

articleSchema.methods.setContent = function(content) {
    this.content = content;
}

articleSchema.methods.setDesc = function(desc) {
    this.desc = desc;
}

articleSchema.methods.setOwner = function(desc) {
    this.desc = desc;
}

mongoose.model("Article", articleSchema);