import { Component, OnInit, EventEmitter, Output, Input } from '@angular/core';
import { FormBuilder, FormControl } from '@angular/forms';

@Component({
  selector: 'app-link-form',
  templateUrl: './link-form.component.html',
  styleUrls: ['./link-form.component.scss']
})
export class LinkFormComponent implements OnInit {

  @Input() id: number;
  @Output() remove:EventEmitter<number> =new EventEmitter<number>();
  constructor() { }

  @Input() formValue: string = '';
  form = new FormControl('', null);
  ngOnInit() {
    this.form = new FormControl(this.formValue, null);
  }

  delete(id){
    this.remove.emit(id);
  }

  @Output() value:EventEmitter<string> =new EventEmitter<string>();
  emitValue(event){
    this.value.emit(event.target.value);
  }

  @Output() focus:EventEmitter<number> =new EventEmitter<number>();
  emitFocus(event){
    this.focus.emit(this.id);
  }

}
