let LogIn = (username, email, password, callb) => {
    $.ajax("/login", {data: {username, email, password}, method: "POST"})
        .done(data => callb(data));
}

$(() => {
    $("#forma").submit((event) => {
        let username = event.target[0].value;
        let email = event.target[1].value;
        let password = event.target[2].value;

        localStorage.setItem("username", username);
        localStorage.setItem("email", email);

        event.target[0].value = "";
        event.target[1].value = "";
        event.target[2].value = "";
        event.preventDefault();
        LogIn(username, email, password, (data) => {
            //window.document.clear();
            //window.document.write(data);
            if(data != 'nista'){
                $(location).attr('href', 'profil.html');
            }
        });
    });    
});