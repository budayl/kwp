const port = 3000;
const host = "localhost";
const root = "../client/dist/zavrsni";
const dbURI = "mongodb://localhost:27017/zavrsni";

module.exports = { port, host, root, dbURI }