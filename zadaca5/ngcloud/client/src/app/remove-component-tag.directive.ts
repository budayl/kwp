import { Directive, ElementRef, Renderer2, OnInit } from '@angular/core';

@Directive({
  selector: '[appRemoveComponentTag]'
})
export class RemoveComponentTagDirective {

  elemRef: ElementRef;
  renderer : Renderer2;
 
  constructor(elemRef: ElementRef, renderer: Renderer2) {
      this.elemRef = elemRef;
      this.renderer = renderer;
  }
  
  ngOnInit() {
      const elem = this.elemRef.nativeElement;
      for (let child of elem.children) {
          this.renderer.appendChild(this.renderer.parentNode(elem), child);
      }
      this.renderer.removeChild(this.renderer.parentNode(elem), elem);
  }

}
