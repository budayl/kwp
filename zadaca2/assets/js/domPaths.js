var check = true;
var elem;

window.addEventListener('click', function(event){
    if(check){
        elem = event.target;
        check = false;
    }else{
        let elem2 = event.target;
        if(elem === elem2){
            console.log(elem.tagName.toLowerCase());
        }else{
            let path = min_path(elem, elem2);
            path = getTags(path);
            console.log(path.join(' -> '));
        }
        check = true;
    }
})

function getTags(arr){
    var tags = [];
    for(let i in arr){
        var doc = arr[i];
        tags.push(doc.tagName.toLowerCase());
    }
    return tags;
}

function min_path(elem1, elem2){
    var elem1_parents = get_parents(elem1);
    var elem2_parents = get_parents(elem2);

    var different = 0;
    while(elem1_parents[different] === elem2_parents[different]){
        different++;
    }

    var path = [];
    for(let i = elem1_parents.length -1; i >= different -1; i--){
        path.push(elem1_parents[i]);
    }
    for(let i = different; i < elem2_parents.length; i++){
        path.push(elem2_parents[i]);
    }

    return path;
}

function get_parents(node){
    var ancestors = [node];
    while(ancestors[0] !== null) {
        ancestors.unshift(ancestors[0].parentElement);
    }
    return ancestors;
}