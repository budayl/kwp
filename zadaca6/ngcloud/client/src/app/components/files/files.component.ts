import { Component, OnInit } from '@angular/core';

import { FileMetadata } from '../../classes/file-metadata';
import { from } from 'rxjs';

import { BsModalService } from 'ngx-bootstrap/modal';
import { UploadModalComponent } from '../../upload-modal/upload-modal.component';
import { FileService } from 'src/app/file.service';

@Component({
  selector: 'app-files',
  templateUrl: './files.component.html',
  styleUrls: ['./files.component.css']
})
export class FilesComponent implements OnInit {
  
  constructor(public modalService: BsModalService, public fileService: FileService) { }

  private files :FileMetadata[];
  ngOnInit() {
    this.fileService.files.subscribe((files: FileMetadata[]) => { 
      this.files = files; 
    })
  }

  openModal() {
    this.modalService.show(UploadModalComponent);
  } 

  delete(file: FileMetadata): void{
    this.fileService.delete(file).subscribe();
  }

  public sorting: string = "added";
  public descending: boolean = true;

  sortToggle(sortBy: string){
    if(this.sorting == sortBy){
      this.descending = !this.descending;
    }else{
      this.descending = true;
      this.sorting = sortBy;
    }

    let desc = "desc";
    if(!this.descending){
      desc = "asc";
    }

    this.fileService.sortingOn(sortBy+"."+desc);
  }
}
