const UserController = require("../controllers/user.controller");

const express = require("express");
let router = express.Router();
const authMiddleware=require('../utils/jwt').authMiddleware;

router.route("/")
    .post(UserController.register);

router.route("/login")
    .post(UserController.login);

router.route("/:id")
    .get(authMiddleware, UserController.findById)
    .post(authMiddleware, UserController.edit);

router.route("/follow/:id")
    .get(authMiddleware, UserController.follow);

router.route("/unfollow/:id")
    .get(authMiddleware, UserController.unfollow);

module.exports = router;


