const FileService = require('../services/file.service');

const mongoose = require("mongoose");
const objectId = mongoose.Types.ObjectId;
const Readable = require("stream").Readable;

let bucket;
mongoose.connection.on("connected", () => {
    bucket = new mongoose.mongo.GridFSBucket(mongoose.connection.db);
});

class FileController {
    static async upload(req, res) {
        let file = req.files.file;
        let contentType = file.mimetype;
        let filename = file.name;
        let owner = objectId(req.payload._id);
        let metadata = { owner };

        const readableStream = new Readable();
        readableStream.push(file.data);
        readableStream.push(null);

        let uploadStream = bucket.openUploadStream(filename, { contentType, metadata });
        readableStream.pipe(uploadStream);

        let message;
        uploadStream.on("error", () => {
            message = "Error when uploading file";
            return res.status(500).json({ message });
        });

        uploadStream.on("finish", async () => {
            try {
                message = "File uploaded";
                let fileInfo = await FileService.findFileById(uploadStream.id);
                return res.status(201).json({ message, fileInfo });
            } catch(err) {
                return res.status(500).json(err);
            }
        });
    }

    static async getFiles(req, res) {
        //todo  Length
        let files;
        let pages;
        let sorting;
        let desc;
        let sortValues = req.query.sort.split(".");
        if (sortValues[1] == "desc"){
            desc = 1;
        }else{
            desc = -1;
        }

        if(sortValues[0] == "name"){
            sorting = { filename : desc};
        }else if(sortValues[0] == "added"){
            sorting = { uploadDate : desc};
        }else{
            sorting = { length : desc};
        }

        try {
            files = await FileService.findFileByOwnerId(req.payload._id,sorting);
            pages = Math.ceil(files.length / 5);
            files = files.slice((req.query.pageNum-1)*5,req.query.pageNum*5);
        } catch(err) {
            return res.status(500).json(err);
        }

        let message = "Sending files";
        return res.status(200).send({ files, message, pages });
    }

    static async download(req, res) {
        console.log("shit");
        let id = req.params.id;
        let file, message;

        try {
            file = await FileService.findFileById(id);
        } catch(err) {
            return res.status(500).json(err);
        }

        if (!file) {
            message = "Not found";
            return res.status(404).json({message});
        }

        let downloadStream = bucket.openDownloadStream(objectId(id));

        res.writeHead(200,{"content-type": file.contentType});
        downloadStream.pipe(res);
    }

    static async delete (req, res) {
        let id = req.params.id;
        let message, file;
        
        try {
            file = await FileService.deleteFileById(id);
        } catch(err) {
            return res.status(500).json({ err });
        }

        if (!file) {
            message = "File not found"
            return res.status(500).json(err);
        }

        message = "Successfully removed!";
        return res.status(200).json({message});
    }
}

module.exports = FileController;