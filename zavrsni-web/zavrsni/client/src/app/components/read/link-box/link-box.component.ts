import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-link-box',
  templateUrl: './link-box.component.html',
  styleUrls: ['./link-box.component.scss']
})
export class LinkBoxComponent implements OnInit {

  @Input() value: string;
  constructor() { }

  ngOnInit() {
  }

}
