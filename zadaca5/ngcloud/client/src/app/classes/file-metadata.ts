export class FileMetadata {
    
    public name: string;
    public added: string;
    public size: number;

    constructor(name: string, added: string, size: number) {
        this.name = name;
        this.added = added;
        this.size = size;
    }
}
