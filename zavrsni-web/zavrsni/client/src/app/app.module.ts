import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import { AppComponent } from './app.component';

import { CollapseModule } from 'ngx-bootstrap';
import { NavbarComponent } from './components/navbar/navbar.component';
import { RemoveHostDirective } from './directives/remove-host.directive';
import { AccountComponent } from './components/account/account.component';
import { LoginComponent } from './components/login/login.component';
import { RegisterComponent } from './components/register/register.component';

import { AppRouter } from './app.routes';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
 
import { ToastrModule } from 'ngx-toastr';


import { ModalModule } from 'ngx-bootstrap/modal';
import { ProgressbarModule } from 'ngx-bootstrap/progressbar';
import { ArticleComponent } from './components/article/article.component';
import { QuoteFormComponent } from './components/article/quote-form/quote-form.component';
import { TitleFormComponent } from './components/article/title-form/title-form.component';
import { SubtitleFormComponent } from './components/article/subtitle-form/subtitle-form.component';
import { LinkFormComponent } from './components/article/link-form/link-form.component';
import { TextFormComponent } from './components/article/text-form/text-form.component';
import { ReadComponent } from './components/read/read.component';
import { FeedComponent } from './components/feed/feed.component';
import { ArticleCardComponent } from './components/article-card/article-card.component';
import { TitleBoxComponent } from './components/read/title-box/title-box.component';
import { SubtitleBoxComponent } from './components/read/subtitle-box/subtitle-box.component';
import { LinkBoxComponent } from './components/read/link-box/link-box.component';
import { QuoteBoxComponent } from './components/read/quote-box/quote-box.component';
import { TextBoxComponent } from './components/read/text-box/text-box.component';
import { CommentCardComponent } from './components/read/comment-card/comment-card.component';

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    RemoveHostDirective,
    AccountComponent,
    LoginComponent,
    RegisterComponent,
    ArticleComponent,
    QuoteFormComponent,
    TitleFormComponent,
    SubtitleFormComponent,
    LinkFormComponent,
    TextFormComponent,
    ReadComponent,
    FeedComponent,
    ArticleCardComponent,
    TitleBoxComponent,
    SubtitleBoxComponent,
    LinkBoxComponent,
    QuoteBoxComponent,
    TextBoxComponent,
    CommentCardComponent
  ],
  imports: [
    BrowserModule,
    CollapseModule.forRoot(),
    AppRouter,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    BrowserAnimationsModule, // required animations module
    ToastrModule.forRoot(), // ToastrModule added,
    ProgressbarModule.forRoot(),
    ModalModule.forRoot()
  ],
  providers: [],
  bootstrap: [AppComponent],
  entryComponents: [
  ]
})
export class AppModule { }
