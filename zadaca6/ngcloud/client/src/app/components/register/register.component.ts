import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';

import { Credentials, UserService } from '../../services/user.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  constructor(public userService: UserService) { }

  userForm: FormGroup;


  ngOnInit() {
    this.userForm = new FormGroup({
      username: new FormControl("jmaltar", [Validators.required]),
      email: new FormControl("jmaltar@mathos.hr", [Validators.required, Validators.email]),
      password: new FormControl("123", [Validators.required])
    });
  }

  onSubmit() {
    let value: Credentials = this.userForm.value;
    this.userService.register(value)
      .subscribe(data => {
        console.log(data);
      })

  }

}
