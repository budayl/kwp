import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { UserService, User } from 'src/app/services/user.service';
import { FormControl } from '@angular/forms';

@Component({
  selector: 'app-read',
  templateUrl: './read.component.html',
  styleUrls: ['./read.component.scss']
})
export class ReadComponent implements OnInit {

  constructor(
    private activatedRoute: ActivatedRoute,
    private http: HttpClient,
    public userService: UserService,
    public router: Router
  ) { }

  article:any;
  id: any;
  myPost: boolean = false;
  me: User;
  ngOnInit() {
    this.id = this.activatedRoute.snapshot.paramMap.get('id');

    this.userService.user.subscribe((user: User) => {
      this.me = user;
    });

    let url = "api/article";
    let headers = this.userService.authHeaders();
    this.http.get(url + `/${this.id}`, { headers }) 
      .subscribe((res: any) => {
        console.log(res);
        this.article = res.article;

        this.userService.user.subscribe((user: User) => {
          if(res.article.owner.id == user._id){
            this.myPost = true;
          }
        });
      }, err => console.log(err));
  }

  visitUser(){
    this.router.navigate(["/account", this.article.owner.id]);
  }

  Edit(){
    this.router.navigate(["/article", this.id]);
  }

  comment = new FormControl('', null);
  postComment(event){
    this.comment.value;

    let url = "api/article/comment";
    let headers = this.userService.authHeaders();
    this.http.post(url + `/${this.id}`, {"content" : this.comment.value, "owner": {"username": this.me.username, "id": this.me._id}} , { headers }) 
      .subscribe((res: any) => {
        this.article.comments.push(res.comment);
        this.comment.setValue('');
      }, err => console.log(err));
  }

}
