let fillList = (messages) => {
    let ul = $("#DisplayMessages")
    ul.empty();
    let len = 0;
    if(messages.length < 6){
        len = messages.length;
        $("#ZaSakrit").hide(); 
    }else{
        len = 5;
        $("#ZaSakrit").show(); 
    }
    for(let i = 0; i < len; i++) {
        let message = messages[i];

        ul.append(`<li><p>${message}</p></li>`)
    }    
}

let NewMessage = (message, callb) => {
    $.ajax("/newmessage", {data: {message}, method: "POST"})
        .done(data => callb(data));
}

let getMessages = (callb) => {
    $.ajax("/message")
        .done(data => callb(data));
}

let getImages = (callb) => {
    $.ajax("/images")
        .done(data => callb(data));
}

$(() => {
    var covImg = 0;
    var profImg = 0;
    $("#UsernameHolder").text(localStorage.getItem("username"));
    $("#EmailHolder").text(localStorage.getItem("email"));
    //$('#Cover-Img').attr('src','C:\Users\buday\Desktop\kwp\zadaca3\client\assets\img\574726244ce1c8569b0c4ddca9bccc99');

    setInterval(getImages((data) => {
        if(data[0] != 0 && data[0] != covImg){
            $("#Cover-Img").attr('src', "assets/img/"+data[0]+".jpg");
            covImg = data[0];
        }
        
        if(data[1] != 0 && data[1] != profImg){
            $("#Profile-Img").attr('src', "assets/img/"+data[1]+".jpg");
        }
    }), 2000);

    

    getMessages((data) => {
        fillList(data.tosend);
    });

    $("#TypeMessage").keyup((event) => {  
        let message = $("#TypeMessage").val();
        let len = message.length;
        if(len < 500){
            $("#CharNum").text((500-len) + ' characters left');
            document.getElementById("SendMessage").disabled = false;
        }else{
            $("#CharNum").text('Message too long - Unable to submit');
            document.getElementById("SendMessage").disabled = true;
        }
    })

    $("#MessageForm").submit((event) => {
        let message = event.target[0].value;
        event.preventDefault();

        NewMessage(message, (data) => {
            fillList(data.tosend);
        });
    });  

    $(document).on('change',"#CoverFile", (event) => {
        event.preventDefault();
        document.getElementById("uploadCover").submit();
        //setTimeout( $('#Cover-Img').attr('src', "assets/img/"+imgCount+".jpg") , 2000);
        //imgCount += 1;
        //var file = $("#CoverFile").get(0).files[0];
        //console.log(file);

        //$('#my_image').attr('src','C:\Users\buday\Desktop\kwp\zadaca3\client\assets\img\574726244ce1c8569b0c4ddca9bccc99');
        /*$('#uploadCover').submit( $.ajax("/addCover",{ 
            data: file , 
            contentType: 'application/json',
            method: "POST",
            success: function(response){
              console.log('image uploaded and form submitted');     
            }
        }));*/
    });

    $(document).on('change',"#ProfileFile", (event) => {
        event.preventDefault();
        document.getElementById("uploadProfile").submit();
    });
});

