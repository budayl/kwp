import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'getIcon'
})
export class GetIconPipe implements PipeTransform {

  transform(name: string): string {
    return "./assets/img/"+name.split(".")[1]+".png";
  }

}
