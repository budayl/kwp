import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-comment-card',
  templateUrl: './comment-card.component.html',
  styleUrls: ['./comment-card.component.scss']
})
export class CommentCardComponent implements OnInit {

  constructor(public router: Router) { }

  @Input() comment: any;
  ngOnInit() {
  }

  visitUser(){
    this.router.navigate(["/account", this.comment.owner.id]);
  }

}
