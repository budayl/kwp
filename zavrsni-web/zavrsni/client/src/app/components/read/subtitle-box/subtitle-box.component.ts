import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-subtitle-box',
  templateUrl: './subtitle-box.component.html',
  styleUrls: ['./subtitle-box.component.scss']
})
export class SubtitleBoxComponent implements OnInit {

  @Input() value: string;
  constructor() { }

  ngOnInit() {
  }

}
