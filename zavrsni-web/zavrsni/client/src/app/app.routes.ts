import { Routes, RouterModule, Router } from '@angular/router';
import { AccountComponent } from './components/account/account.component';
import { LoginComponent } from './components/login/login.component';
import { RegisterComponent } from './components/register/register.component';
import { ArticleComponent } from './components/article/article.component';
import { NgModule } from '@angular/core';
import { FeedComponent } from './components/feed/feed.component';
import { ReadComponent } from './components/read/read.component';

const routes: Routes = [
    { path: "", redirectTo: "feed", pathMatch: "full" },
    { path: "account/:id", component: AccountComponent },
    { path: "account", component: AccountComponent },
    { path: "login", component: LoginComponent },
    { path: "register", component: RegisterComponent },
    { path: "article/:id", component: ArticleComponent },
    { path: "article", component: ArticleComponent },
    { path: "feed", component: FeedComponent },
    { path: "read/:id", component: ReadComponent },
    { path: "**", redirectTo: "feed" }
]

@NgModule({
    imports: [ RouterModule.forRoot(routes) ],
    exports: [ RouterModule ]
})
export class AppRouter {};