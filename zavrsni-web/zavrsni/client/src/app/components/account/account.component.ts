import { Component, OnInit } from '@angular/core';
import { FileMetadata } from 'src/app/classes/file-metadata';
import { BsModalService } from 'ngx-bootstrap';
import { UserService, User } from 'src/app/services/user.service';
import { ActivatedRoute, Router } from '@angular/router';
import { HttpClient, HttpParams } from '@angular/common/http';
import { FormBuilder, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-account',
  templateUrl: './account.component.html',
  styleUrls: ['./account.component.scss']
})
export class AccountComponent implements OnInit {

  constructor(
    public router: Router,
    public userService: UserService,
    private activatedRoute: ActivatedRoute,
    private http: HttpClient,
    private fb: FormBuilder
    ) { }

  private files :FileMetadata[];
  user: User = null;
  userForm: FormGroup;
  id;
  articles: any[] = [];
  myAccount: boolean= false;
  currPage:number = 1;
  ngOnInit() {
    this.id = this.activatedRoute.snapshot.paramMap.get('id');

    if(!this.id){
      this.userService.user.subscribe((user: User) => {
        this.user = user;
        this.id = user._id;
        this.myAccount = true;
        console.log(user.desc);
        this.userForm = this.fb.group({
          "username": user.username,
          "desc": user.desc
        });
      });
    }
    /*
    this.fileService.files.subscribe((files: FileMetadata[]) => { 
      this.files = files; 
    })*/

    let url = "api/article/user";
    let params = new HttpParams().set("pageNum",`${this.currPage}`);
    let headers = this.userService.authHeaders();
    this.http.get(url + `/${this.id}`, { headers, params }) 
      .subscribe((res: any) => {
        this.articles = res.articles;
      }, err => console.log(err));

    url = "api/user";
    this.http.get(url + `/${this.id}`, { headers }) 
      .subscribe((res: any) => {
        console.log("getting user", res);
        this.user = res.user;
      }, err => console.log(err));
  }

  createArticle() {
    this.router.navigate(["/article"]);
  } 

  followUser(){
    let url = "api/user/follow";
    let headers = this.userService.authHeaders();
    this.http.get(url + `/${this.id}`, { headers }) 
      .subscribe((res: any) => {
        console.log("followed");
        this.user.followBool = true;
        this.user.followers.number += 1;
      }, err => console.log(err));
  }

  unfollowUser(){
    let url = "api/user/unfollow";
    let headers = this.userService.authHeaders();
    this.http.get(url + `/${this.id}`, { headers }) 
      .subscribe((res: any) => {
        console.log("unfollowed");
        this.user.followBool = false;
        this.user.followers.number -= 1;
      }, err => console.log(err));
  }

  nextPage(){
    console.log("here");
    if(this.articles.length >10){
      this.currPage +=1
      let url = "api/article/user";
      let headers = this.userService.authHeaders();
      let params = new HttpParams().set("pageNum",`${this.currPage}`);
      this.http.get(url + "/" + this.id, {headers, params}).subscribe((data:any) => {
        console.log(data);
        this.articles = data.articles;
      }, err => console.log(err));
    }
  }

  prevPage(){
    if(this.currPage != 1){
      this.currPage -=1
      let url = "api/article/user";
      let headers = this.userService.authHeaders();
      let params = new HttpParams().set("pageNum",`${this.currPage}`);
      this.http.get(url + "/"+ this.id, {headers, params}).subscribe((data:any) => {
        console.log(data);
        this.articles = data.articles;
      }, err => console.log(err));
    }
  }

  editing: boolean = false;
  editUser(){
    this.editing = !this.editing;

    if(!this.editing){
      let url = "api/user";
      let headers = this.userService.authHeaders();
      let value = this.userForm.value;

      console.log(value);

      this.http.post(url + "/" + this.id, { value },{ headers }).subscribe((data:any) => {
        console.log(data);
        this.userService.user.next(data.user);
        this.user.username = data.user.username;
        this.user.desc = data.user.desc;
      }, err => console.log(err));
    }
  }

}
