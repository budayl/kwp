const mongoose = require('mongoose');
const Article = mongoose.model('Article');
const User = mongoose.model('User');

class ArticleService {
    static async createArticle(title, desc, content, owner) {
        let user = await User.findById(owner).exec();
        let article = new Article({ "title": title, "desc": desc, "content": content, "owner": { "id": owner, "username": user.username }, "uploadDate": Date.now(), "comments": [] });
        
        await article.save();

        try{
            user.articles.push(article._id);
        }catch{
            user.articles = [];
            user.articles.push(article._id);
        }
        
        user.save();

        return article;
    }

    static async editArticle(_id, title, desc, content, userId) {
        const update = { title, desc, content };

        let article = await Article.findById({ _id }).exec();
        if(userId == article.owner.id){
            article.title = title;
            article.desc = desc;
            article.content = content;
            article.save();
        }else{
            throw 'not authorised';
        }

        return article;
    }

    static async comment(_id, content, owner) {

        let article = await Article.findById({ _id }).exec();
        article.comments.push({ "content": content, "owner": owner })
        article.save();

        return { "content": content, "owner": owner };
    }

    static async getArticleById(_id) {
        return await Article.findById({ _id }).exec();
    }

    static async getArticlesByUserId(_id, page= 1){
        return await Article.find({ "owner.id" : _id }).sort('-uploadDate').limit((10*(page))+1).skip(10 * (page-1)).exec();
    }

    static async getFeed(page){
        return await Article.find().sort('-uploadDate').limit((10*(page))+1).skip(10 * (page-1)).exec(); //
    }
}

module.exports = ArticleService;