import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpEventType, HttpRequest, HttpParams } from '@angular/common/http';
import { UserService } from './services/user.service';

import { throwError, Observable, BehaviorSubject } from 'rxjs';
import { map, tap, last, catchError } from 'rxjs/operators';
import { ToastrService } from 'ngx-toastr';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { FileMetadata } from './classes/file-metadata';

@Injectable({
  providedIn: 'root'
})
export class FileService {

  public pages: BehaviorSubject<number> = new BehaviorSubject<number>(1);
  public currPage: BehaviorSubject<number> = new BehaviorSubject<number>(1);
  public files: BehaviorSubject<FileMetadata[]> = new BehaviorSubject<FileMetadata[]>([]);
  public sortBy: BehaviorSubject<string> = new BehaviorSubject<string>("added.desc");
  
  sortingOn(str: string) {
    this.sortBy.next(str);
    this.getFiles().subscribe(data => {this.files.next(data.files); this.pages.next(data.pages)}, err => this.handleError(err));
  }

  fileUrl: string = "/api/files";
  
  constructor(
    public httpClient: HttpClient, 
    public userService: UserService
  ) { 
    this.userService.loggedIn.subscribe((loggedIn: boolean) => {
      if(loggedIn) this.getFiles().subscribe(data => {this.files.next(data.files); this.pages.next(data.pages)}, err => this.handleError(err))
      else {console.log("Unauthorised"); this.files.next([])};
    });
  }

  private handleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      // A client-side or network error occurred. Handle it accordingly.
      console.error('An error occurred:', error.error.message);
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong,
      console.error(
        `Backend returned code ${error.status}, ` +
        `body was: ${error.error}`);
    }

    // return an observable with a user-facing error message
    return throwError(
      'Something bad happened; please try again later.');

    
  };

  uploadStatus(event: any): { percentage: number, done: boolean } {
    if(event.body) {
      this.getFiles().subscribe(data => {this.files.next(data.files); this.pages.next(data.pages)}, err => this.handleError(err));
    }
    
    let percentage = Math.round(100 * event.loaded / event.total);
    let done = HttpEventType.Response == event.type;

    return { percentage, done };
  }

  upload(file: any, dataHandler: (percentage: number, done: boolean) => any) {
    let formData: FormData = new FormData();
    formData.append('file', file, file.name);

    let headers = this.userService.authHeaders();

    const req = new HttpRequest(
      'POST',
      this.fileUrl,
      formData,
      { reportProgress: true, headers }
    );

    return this.httpClient.request(req)
      .pipe(
        map(event => this.uploadStatus(event)),
        tap((status: { percentage: number, done: boolean }) => dataHandler(status.percentage, status.done)),
        last(),
        catchError(this.handleError)
      );
  }

  getFiles(): Observable<{ files: FileMetadata[], message: string, pages: number }> {
    let headers = this.userService.authHeaders();
    let params = new HttpParams().set("sort",`${this.sortBy.value}`).set("pageNum",`${this.currPage.value}`);
    return this.httpClient.get<{ files: FileMetadata[], message: string, pages: number }>(this.fileUrl, {headers,params});
  }

  delete(file: FileMetadata): any {
    let headers = this.userService.authHeaders();
    return this.httpClient.delete(`${this.fileUrl}/${file._id}`,{ headers })
      .pipe(
        tap(event => {
          let files = this.files.getValue();
          if (files.length == 1) this.currPage.next((this.currPage.value != 1) ? this.currPage.value-1 : 1);
          this.getFiles().subscribe(data => {this.files.next(data.files); this.pages.next(data.pages)}, err => this.handleError(err));
        }),
        catchError(this.handleError)
      );
  }

  listPages():Array<number>{
    return Array<number>(this.pages.value).fill(1,0,42).map((x,i)=>x+i);
  }

  nextPage(){
    if (this.currPage.value != this.pages.value) this.currPage.next(this.currPage.value+1);
    this.getFiles().subscribe(data => {this.files.next(data.files); this.pages.next(data.pages)}, err => this.handleError(err));
  }

  prevPage(){
    if (this.currPage.value != 1) this.currPage.next(this.currPage.value-1);
    this.getFiles().subscribe(data => {this.files.next(data.files); this.pages.next(data.pages)}, err => this.handleError(err));
  }

  goToPage(n: number) {
    this.currPage.next(n);
    this.getFiles().subscribe(data => {this.files.next(data.files); this.pages.next(data.pages)}, err => this.handleError(err));
  }
}
