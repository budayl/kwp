let fillList = (messages) => {
    let ul = $("#DisplayMessages")
    ul.empty();
    for(let i = 0; i < messages.length; i++) {
        let message = messages[i];

        ul.append(`<li><p>${message}</p></li>`)
    }    
}

let getAllMessages = (callb) => {
    $.ajax("/allmessage")
        .done(data => callb(data));
}

$(() => {
    getAllMessages((data) => {
        fillList(data.messages);
    });
});