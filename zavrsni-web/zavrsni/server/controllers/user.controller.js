const UserService = require("../services/user.service");


class UserController {
    static async register(req, res) {
        let username = req.body.username;
        let email = req.body.email;
        let password = req.body.password;

        let user, token, message;

        try {
            user = await UserService.createUser(username, email, password);
        } catch(err) {
            return res.status(500).json(err);
        }

        message = "Successfully registered";
        token = user.generateJWT();

        return res.status(201).json({ message, token })
    }

    static async login(req, res) {
         let email = req.body.email;
         let password = req.body.password;

         let user, message, token;

         try {
            user = await UserService.getUserByEmail(email);
         } catch(err) {
             return res.status(500).json(err);
         }

         if (!user || !user.validPassword(password)) {
            message = "Incorrect email or password";
            return res.status(404).json({ message });
        }
        
        token = user.generateJWT();
        message = "Successfully logged in!";
        return res.status(202).json({ token, message });
    }

    static async findById(req, res){
        let id = req.params.id;
        let user, message;

        try {
            user = await UserService.getUserById(id);
            user = JSON.parse(JSON.stringify(user));
            user["hash"] = undefined;
            user["salt"] = undefined;
        } catch(err) {
            return res.status(500).json(err);
        }

        if(user.followers.ids.includes(req.payload._id)){
            user["followBool"] = true;
        }else{
            user["followBool"] = false;
        }

        if (!user) {
            message = "Not found";
            return res.status(404).json({message});
        }

        return res.status(201).json({ message, user })
    }

    static async edit(req, res){
        console.log("inside edit");
        let id = req.params.id;
        let _id = req.payload._id;
        let username = req.body.value.username;
        let desc = req.body.value.desc;
        let message, user;
        console.log(req);

        try {
            user = await UserService.edit(_id, id, username, desc);
            user = JSON.parse(JSON.stringify(user));
            user["hash"] = undefined;
            user["salt"] = undefined;
        } catch(err) {
            console.log(err);
            return res.status(500).json(err);
        }

        return res.status(201).json({ user, message })
    }

    static async follow(req, res){
        console.log("inside follow");
        let id = req.params.id;
        let _id = req.payload._id;
        let message;

        try {
            await UserService.follow(_id, id);
        } catch(err) {
            console.log(err);
            return res.status(500).json(err);
        }

        return res.status(201).json({ message })
    }

    static async unfollow(req, res){
        let id = req.params.id;
        let _id = req.payload._id;
        let message;

        try {
            await UserService.unfollow(_id, id);
        } catch(err) {
            return res.status(500).json(err);
        }

        return res.status(201).json({ message })
    }
}


module.exports = UserController;