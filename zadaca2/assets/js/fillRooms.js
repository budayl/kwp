class Room{
    constructor(name, itemsCount){
        this.name = name;
        this.itemsCount = itemsCount;
    }
}

var rooms = [new Room('Predavaonica 1', 50),
               new Room('Predavaonica 2', 50),
               new Room('Predavaonica 3', 50),
               new Room('Predavaonica 4', 50),
               new Room('Referada', 50),
               new Room('Predavaonica 5', 50),];

(function(){
    let ul = document.getElementById("pred");
    for(let room of rooms){
        let a = document.createElement("a");
        let p1 = document.createElement("p");
        let p2 = document.createElement("p");
        p1.setAttribute("class", "head_room" );
        p1.appendChild(document.createTextNode(room.name));
        p2.appendChild(document.createTextNode("Broj predmeta: " + room.itemsCount));
        a.appendChild(p1)
        a.appendChild(p2);
        a.href = '#';
        let li = document.createElement("li");
        li.appendChild(a);
        ul.appendChild(li);
    }
})();

function getRandomInt(max) {
    return Math.floor(Math.random() * Math.floor(max));
  }

function isEven(n) {
    n = Number(n);
    return n === 0 || !!(n && !(n%2));
}

function sortiraj(){
    ul = document.getElementById("pred");
    img = document.getElementById("strelica");

    while (ul.firstChild) {
        ul.removeChild(ul.firstChild);
    }

    if(isEven(getRandomInt(2))){
        rooms.sort((a, b) => (a.name > b.name) ? 1 : -1);
        img.setAttribute("style", "transform:none; width: 10px; height: 8px; position: absolute; left: 415px; padding:8px 0px 0px 10px;" );
    }else{
        rooms.sort((a, b) => (a.name > b.name) ? -1 : 1);
        img.setAttribute("style", "transform: rotate(180deg); width: 10px; height: 8px; position: absolute; left: 425px; top: 69px; padding:8px 0px 0px 10px;" );
    }
    

    for(let room of rooms){
        let a = document.createElement("a");
        let p1 = document.createElement("p");
        let p2 = document.createElement("p");
        p1.setAttribute("class", "head_room" );
        p1.appendChild(document.createTextNode(room.name));
        p2.appendChild(document.createTextNode("Broj predmeta: " + room.itemsCount));
        a.appendChild(p1)
        a.appendChild(p2);
        a.href = '#';
        let li = document.createElement("li");
        li.appendChild(a);
        ul.appendChild(li);
    }
}