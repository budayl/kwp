const mongoose = require("mongoose");
const File = mongoose.model("File");
const Chunk = mongoose.model("Chunk");

const objectId = mongoose.Types.ObjectId;

class FileService {
    static async findFileById(id) {
        let _id = objectId(id);
        return await File.findOne({ _id }).exec();
    }

    static async findFileByOwnerId(id, sorting){
        return await File.find({ "metadata.owner": id }).sort(sorting).exec();
    }

    static async deleteFileById(id){
        await Chunk.deleteMany({ files_id: id }).exec();
        return await File.deleteOne({ _id: id}).exec();
    }
}

module.exports = FileService;