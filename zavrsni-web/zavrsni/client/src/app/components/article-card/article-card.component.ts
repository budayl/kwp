import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-article-card',
  templateUrl: './article-card.component.html',
  styleUrls: ['./article-card.component.scss']
})
export class ArticleCardComponent implements OnInit {

  @Input() article: any;
  constructor(public router: Router) { }

  ngOnInit() {
  }

  navigate(){
    this.router.navigate(['/read', this.article._id]);
  }

  visitUser(){
    this.router.navigate(["/account", this.article.owner.id]);
  }
}
