import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormControl } from '@angular/forms';

@Component({
  selector: 'app-subtitle-form',
  templateUrl: './subtitle-form.component.html',
  styleUrls: ['./subtitle-form.component.scss']
})
export class SubtitleFormComponent implements OnInit {

  @Input() id: number;
  @Output() remove:EventEmitter<number> =new EventEmitter<number>();
  constructor() { }

  @Input() formValue: string = '';
  form = new FormControl('', null);
  ngOnInit() {
    this.form = new FormControl(this.formValue, null);
  }

  delete(id){
    this.remove.emit(id);
  }

  @Output() value:EventEmitter<string> =new EventEmitter<string>();
  emitValue(event){
    this.value.emit(event.target.value);
  }

  @Output() focus:EventEmitter<number> =new EventEmitter<number>();
  emitFocus(event){
    this.focus.emit(this.id);
  }
}
