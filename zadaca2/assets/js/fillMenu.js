class Image{
    constructor(name, alt){
        this.name = name;
        this.alt = alt;
    }
}

class Action{
    constructor(name, image, link) {
    this.name = name;
    this.image = image;
    this.link = link;
    }
}

var actions = [new Action('Inventura',new Image('News.svg', 'male novine'), '#inventura'),
               new Action('Inventar',new Image('Box.svg', 'mala kutija'), '#inventar'),
               new Action('Prostorije',new Image('Classroom.svg', 'ucinonica'), '#prostorije'),
               new Action('Zaposlenici',new Image('Contacts.svg', 'ikona osobe'), '#zaposlenici'),
               new Action('Administracija',new Image('Services.svg', 'zupcanici'), '#administracija'),];

(function(){
    let ul = document.getElementById("actions");
    for(let action of actions){
        let a = document.createElement("a");
        let img = document.createElement("img");
        img.setAttribute("style", "width: 20px; height: 20px;" );
        img.setAttribute("src", "assets/img/" + action.image.name);
        a.appendChild(img);
        a.appendChild(document.createTextNode(action.name));
        a.href = action.link;
        let li = document.createElement("li");
        li.appendChild(a);
        ul.appendChild(li);
    }
})();