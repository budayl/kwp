const express=require('express');

let router=express.Router();
const authMiddleware=require('../utils/jwt').authMiddleware;

const FileController=require('../controllers/file.controller');

router.route('')
    .get(authMiddleware, FileController.getFiles)
    .post(authMiddleware, FileController.upload);

router.route('/:id')
    .get(FileController.download)
    .delete(FileController.delete);

module.exports=router;