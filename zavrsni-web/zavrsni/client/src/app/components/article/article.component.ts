import { Component, OnInit } from '@angular/core';
import { FormBuilder, AbstractFormGroupDirective } from '@angular/forms';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { UserService } from 'src/app/services/user.service';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-article',
  templateUrl: './article.component.html',
  styleUrls: ['./article.component.scss']
})
export class ArticleComponent implements OnInit {

  //MAX AMOUNT OF CHARACTERS IN DESCRIPTION IS 116

  titleForm;

  constructor(
    private activatedRoute: ActivatedRoute,
    private formBuilder: FormBuilder,
    public http: HttpClient,
    public userService: UserService,
    public router: Router
  ) {
    this.titleForm = this.formBuilder.group({
      title: '',
      desc: ''
    });
  }

  id;
  article: any;
  ngOnInit() {
    this.id = this.activatedRoute.snapshot.paramMap.get('id');

    if(this.id){
      let url = "api/article";
      let headers = this.userService.authHeaders();
      this.http.get(url + `/${this.id}`, { headers }) 
        .subscribe((res: any) => {
          console.log(res);
          this.article = res.article;
          for(let elem of this.article.content){
            elem["id"] = this.currentId;
            this.currentId +=1;
            this.contentArray.push(elem);
          }
          console.log(this.contentArray);
          this.titleForm = this.formBuilder.group({
            title: this.article.title,
            desc: this.article.desc
          });
        }, err => console.log(err));
    }
  }

  contentArray = [];
  currentId: number = 0;

  addNew(string: string){
    let newForm = { "type" : string, "id" : this.currentId, "value": ""}
    this.currentId += 1;
    
    this.contentArray.push(newForm);
    this.currentForm = newForm;
  }

  remove(id){
    for(let elem of this.contentArray){
      if(elem.id == id){
        this.contentArray.splice(this.contentArray.indexOf(elem), 1);
        break;
      }
    }
  }

  currentForm: any;
  update(value: string){
    this.currentForm.value = value;
  }

  changeFocus(id){
    for(let elem of this.contentArray){
      if(elem.id == id){
        this.currentForm = elem;
        break;
      }
    }
  }


  private url: string = "/api/article";
  Save(){
    console.log("uso u save");
    let title = this.titleForm.get("title").value;
    let desc = this.titleForm.get("desc").value;
    let content = this.contentArray.map(x => Object.assign({}, x));
    for(let elem of content){
      delete elem["id"];
    }
    
    let headers = this.userService.authHeaders();
    if(this.id){
      this.http.post(this.url +"/edit/" + this.id, {title, desc, content }, { headers }) 
        .subscribe((res: any) => {
          this.router.navigate(["/read", res.article._id]);
        }, err => console.log(err));
    }else{
      this.http.post(this.url, {title, desc, content }, { headers }) 
        .subscribe((res: any) => {
          this.router.navigate(["/read", res.article._id]);
        }, err => console.log(err));
    }
    
  }
}
