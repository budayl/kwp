import { Component } from '@angular/core';

import { FileMetadata } from './classes/file-metadata';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent {

  title = 'zadaca5';

}
