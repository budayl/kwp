import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormControl } from '@angular/forms';

@Component({
  selector: 'app-title-form',
  templateUrl: './title-form.component.html',
  styleUrls: ['./title-form.component.scss']
})
export class TitleFormComponent implements OnInit {

  @Input() id: number;
  @Output() remove:EventEmitter<number> =new EventEmitter<number>();
  @Output() value:EventEmitter<string> =new EventEmitter<string>();
  constructor() { }

  @Input() formValue: string = '';
  form = new FormControl('', null);
  ngOnInit() {
    this.form = new FormControl(this.formValue, null);
  }

  delete(id){
    this.remove.emit(id);
  }

  emitValue(event){
    this.value.emit(event.target.value);
  }

  @Output() focus:EventEmitter<number> =new EventEmitter<number>();
  emitFocus(event){
    this.focus.emit(this.id);
  }
}
