const express = require("express");
const path = require("path");
const bodyParser = require("body-parser");
const morgan = require("morgan");
const busboyBodyParser = require("busboy-body-parser");

const config = require("./config/config");

let app = express();

// povezivanje na bazu podataka
require('./utils/db');
// importamo modele
require("./models");

// sve rute
const routes = require('./routes');

app.use(busboyBodyParser());

app.use(morgan("tiny"));

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

app.use(express.static(path.join(__dirname, config.root)));

app.use("/api", routes);

app.listen(config.port, () => {
    console.log(`I'm listening at ${config.port}`);
});

