import { Component, OnInit } from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap/modal';

import { FormControl, FormGroup, Validators } from '@angular/forms';
import { FileService } from '../file.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-upload-modal',
  templateUrl: './upload-modal.component.html',
  styleUrls: ['./upload-modal.component.css']
})
export class UploadModalComponent implements OnInit {

  constructor(public modal: BsModalRef, public fileService: FileService, public toastrService: ToastrService) { }

  percentage: number = 0;

  dataHandler(percentage: number, done: boolean): void {
    if (!isNaN(percentage))
      this.percentage = percentage;

    if (done) {
      this.modal.hide();
      this.toastrService.success("Successfully uploaded!");
    }
  }

  fileForm: FormGroup;
  ngOnInit() {
    this.fileForm = new FormGroup({
      file: new FormControl(null, [ Validators.required ])
    });
  }

  onFileChange(event): any {
    if (event.target.files && event.target.files.length)
      this.fileForm.patchValue({
        file: event.target.files.item(0)
      });
  }

  upload() {
    this.fileService.upload(
      this.fileForm.controls["file"].value,
      this.dataHandler.bind(this)
    ).subscribe(() => {

    }, (err: any) => {
      this.toastrService.error(err);
      this.modal.hide();
    });
  }

}
