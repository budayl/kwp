import { Component, OnInit } from '@angular/core';
import { FileMetadata } from '../../classes/file-metadata';
import { Pipe, PipeTransform } from '@angular/core';

@Component({
  selector: 'app-files',
  templateUrl: './files.component.html',
  styleUrls: ['./files.component.css']
})
export class FilesComponent implements OnInit {

  constructor() {}

  ngOnInit() {
  }

  files: FileMetadata[] = [
    new FileMetadata("todo.docx", "5/1/2019 8:38 PM", 0.1),
    new FileMetadata("onemore.pdf", "5/1/2019 8:38 PM", 0.2),
    new FileMetadata("chart.xls", "5/1/2019 8:38 PM", 0.3),
    new FileMetadata("chart2.xls", "5/1/2019 8:38 PM", 0.4),
  ];

}
