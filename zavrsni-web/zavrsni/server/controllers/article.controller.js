const ArticleService = require("../services/article.service");

const mongoose = require("mongoose");
const objectId = mongoose.Types.ObjectId;

class ArticleController {
    static async create(req, res) {
        console.log("REQUESTTTT", req.body);
        let title = req.body.title;
        let desc = req.body.desc;
        let content = req.body.content;
        let owner = objectId(req.payload._id);
        console.log(title, desc, content, owner);
        let article, message;

        try {
            article = await ArticleService.createArticle(title, desc, content, owner);
        } catch(err) {
            console.log(err);
            return res.status(500).json(err);
        }

        message = "Successfully created";

        return res.status(201).json({ message, article })
    }

    static async edit(req, res) {

        let title = req.body.title;
        let desc = req.body.desc;
        let content = req.body.content;
        let id = req.params.id;
        let article, message;

        try {
            article = await ArticleService.editArticle(id, title, desc, content, req.payload._id);
        } catch(err) {
            console.log(err);
            return res.status(500).json(err);
        }

        message = "Successfully created";

        return res.status(201).json({ message, article })
    }

    static async comment(req, res) {

        let content = req.body.content;
        let owner = req.body.owner;
        let id = req.params.id;
        let comment, message;

        if(req.payload._id == owner.id){
            try {
                comment = await ArticleService.comment(id, content, owner);
            } catch(err) {
                console.log(err);
                return res.status(500).json(err);
            }

            message = "Successfully created";

            return res.status(201).json({ message, comment })
        }else{
            return res.status(500).json('unauthorised');
        }
        
    }

    static async findById(req, res) {
        let id = req.params.id;
        let article, message;

        try {
            article = await ArticleService.getArticleById(id);
        } catch(err) {
            return res.status(500).json(err);
        }

        if (!article) {
            message = "Not found";
            return res.status(404).json({message});
        }

        return res.status(201).json({ message, article })
    }

    static async findByUserId(req, res) {
        let page = req.query.pageNum;
        let userId = req.params.id;
        let articles, message;
        console.log(userId);

        try {
            articles = await ArticleService.getArticlesByUserId(userId, page);
        } catch(err) {
            console.log(err);
            return res.status(500).json(err);
        }

        if (!articles) {
            message = "Not found";
            return res.status(404).json({message});
        }

        return res.status(201).json({ message, articles })
    }

    static async getFeed(req, res){
        let page = req.query.pageNum;
        let articles;

        try {
            articles = await ArticleService.getFeed(page);
            //articles = articles.slice((req.query.pageNum-1)*10,req.query.pageNum*10);
        } catch(err) {
            console.log(err);
            return res.status(500).json(err);
        }

        let message = "Sending articles";
        return res.status(200).send({ articles, message });
    }
}


module.exports = ArticleController;