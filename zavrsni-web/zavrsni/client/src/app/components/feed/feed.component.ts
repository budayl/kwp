import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-feed',
  templateUrl: './feed.component.html',
  styleUrls: ['./feed.component.scss']
})
export class FeedComponent implements OnInit {

  constructor(
    public http: HttpClient,
    public userService: UserService
  ) { }

  articles: any[] = [];
  currPage: number = 1;
  ngOnInit() {
    let url: string = "/api/article/feed";
    let headers = this.userService.authHeaders();
    let params = new HttpParams().set("pageNum",`${this.currPage}`);
    this.http.get(url, {headers, params}).subscribe((data:any) => {
      console.log(data);
      this.articles = data.articles;
    }, err => console.log(err));
  }

  nextPage(){
    console.log("here");
    if(this.articles.length >10){
      this.currPage +=1
      let url: string = "/api/article/feed";
      let headers = this.userService.authHeaders();
      let params = new HttpParams().set("pageNum",`${this.currPage}`);
      this.http.get(url, {headers, params}).subscribe((data:any) => {
        console.log(data);
        this.articles = data.articles;
      }, err => console.log(err));
    }
  }

  prevPage(){
    if(this.currPage != 1){
      this.currPage -=1
      let url: string = "/api/article/feed";
      let headers = this.userService.authHeaders();
      let params = new HttpParams().set("pageNum",`${this.currPage}`);
      this.http.get(url, {headers, params}).subscribe((data:any) => {
        console.log(data);
        this.articles = data.articles;
      }, err => console.log(err));
    }
  }

}
