var sol = "";
var calccurr = "";
var calcdisplay= "";
var lastIsOper = true;
var doubleOpers = false;
var num = 0;
var first = true;
var opers = ["-", "/", "*", "+", "."];

let getnum = (e) => {
    if(num<20){
        let currnum = $(e.target).text();
        calccurr += currnum;
        calcdisplay += currnum;
        sol = String(eval(calccurr));
        lastIsOper = false;
        num += 1
        doubleOpers = false;
    
        $("#calculating").text(calcdisplay);
        $("#solution").text(sol);
    }else{
        alert("TOO MANY CHARACTERS")
    }
    first = false;
};

$("#key-C").click((event) => {
    sol = "";
    calccurr = "";
    calcdisplay= "";
    lastIsOper = true;
    doubleOpers = false;
    first = true;
    num = 0;

    $("#calculating").text(calcdisplay);
    $("#solution").text(sol);
});

$("#key-div").click((event) => {
    if(!first){
        if(num < 20){
            if(calccurr != "" && !lastIsOper){
                calccurr += "/";
                calcdisplay += "÷";
                lastIsOper = true;
        
                $("#calculating").text(calcdisplay);
                num += 1;
            }else{
                if(!doubleOpers){
                    calccurr = calccurr.substr(0,calccurr.length-1) + "/";
                    calcdisplay = calcdisplay.substr(0,calcdisplay.length-1) + "÷";
                    lastIsOper = true;
        
                    $("#calculating").text(calcdisplay);
                }
            }
            
        }else{
            alert("TOO MANY CHARACTERS")
        }
    } 
});

$("#key-mult").click((event) => {
    if(!first){
        if(num < 20){
            if(calccurr != "" && !lastIsOper){
                calccurr += "*";
                calcdisplay += "x";
                lastIsOper = true;
        
                $("#calculating").text(calcdisplay);
                num += 1;
            }else{
                if(!doubleOpers){
                    calccurr = calccurr.substr(0,calccurr.length-1) + "*";
                    calcdisplay = calcdisplay.substr(0,calcdisplay.length-1) + "x";
                    lastIsOper = true;
        
                    $("#calculating").text(calcdisplay);
                }
            } 
        }else{
            alert("TOO MANY CHARACTERS")
        }
    }
});

$("#key-min").click((event) => {
    let specOpers = ['*','/']
    if(!first){
        if(num < 20){
            if(calccurr != "" && !lastIsOper){
                calccurr += "-";
                calcdisplay += "-";
                lastIsOper = true;
        
                $("#calculating").text(calcdisplay);
                num += 1;
            }else{
                if(specOpers.includes(calccurr[calccurr.length-1])){
                    calccurr += "-";
                    calcdisplay += "-";
                    lastIsOper = true;
                    doubleOpers = true;
        
                    $("#calculating").text(calcdisplay);
                    num += 1;
                }else{
                    calccurr = calccurr.substr(0,calccurr.length-1) + "-";
                    calcdisplay = calcdisplay.substr(0,calcdisplay.length-1) + "-";
                    lastIsOper = true;
        
                    $("#calculating").text(calcdisplay);
                }
            }
            
        }else{
            alert("TOO MANY CHARACTERS")
        }
    }else{
        calccurr += "-";
        calcdisplay += "-";
        lastIsOper = true;
        first = false;

        $("#calculating").text(calcdisplay); 
        num += 1;  
    }
});

$("#key-plus").click((event) => {
    if(!first){
        if(num < 20){
            if(!lastIsOper){
                calccurr += "+";
                calcdisplay += "+";
                lastIsOper = true;
        
                $("#calculating").text(calcdisplay);
                num += 1;
            }else{
                if(!doubleOpers){
                    calccurr = calccurr.substr(0,calccurr.length-1) + "+";
                    calcdisplay = calcdisplay.substr(0,calcdisplay.length-1) + "+";
                    lastIsOper = true;
        
                    $("#calculating").text(calcdisplay);
                }
            }
        }else{
            alert("TOO MANY CHARACTERS")
        }
    } 
});

$("#key-dot").click((event) => {
    if(!first){
        if(num < 20){
            if(!lastIsOper){
                calccurr += ".";
                calcdisplay += ".";
                lastIsOper = true;
        
                $("#calculating").text(calcdisplay);
                num += 1;
            }else{
                if(!doubleOpers){
                    calccurr = calccurr.substr(0,calccurr.length-1) + ".";
                    calcdisplay = calcdisplay.substr(0,calcdisplay.length-1) + ".";
                    lastIsOper = true;
        
                    $("#calculating").text(calcdisplay);
                }
            } 
        }else{
            alert("TOO MANY CHARACTERS")
        } 
    }
    
});

$("#key-equals").click((event) => {
    calccurr = sol;
    calcdisplay= sol;
    lastIsOper = false;

    $("#calculating").text(calcdisplay);
    $("#solution").text(sol);

    num = calccurr.length;
});

$("#key-bs").click((event) => {
    if(num > 0){
        calccurr = calccurr.substr(0,calccurr.length-1);
        calcdisplay = calcdisplay.substr(0,calcdisplay.length-1);
    
        if(opers.includes(calccurr[calccurr.length-1])){
            lastIsOper = true;
            sol = String(eval(calccurr.substr(0,calccurr.length-1)));
            if(opers.includes(calccurr[calccurr.length-2])){
                doubleOpers = true;
            }else{
                doubleOpers = false;
            }
        }else{
            sol = String(eval(calccurr));
            lastIsOper = false;
        }
        if(sol=="undefined"){
            sol = "";
            first = true;
        }
        $("#calculating").text(calcdisplay);
        $("#solution").text(sol);

        num -= 1;
    }    
});

$(() => {
    $("#calculating").text(calcdisplay);
    $("#solution").text(sol);
});