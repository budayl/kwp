const mongoose = require('mongoose');
const User = mongoose.model('User');

const objectId = mongoose.Types.ObjectId;

class UserService {
    static async createUser(username, email, password) {
        let user = new User({ email, username, "following": [], "articles": [], "followers": {"number" : 0, "ids": []}});
        user.setPassword(password);
        user.setGravatar(email);

        await user.save();
        return user;
    }

    static async getUserByEmail(email) {
        return await User.findOne({ email }).exec();
    }

    static async getUserByEmail(email) {
        return await User.findOne({ email }).exec();
    }

    static async getUserById(_id) {
        return await User.findById({ _id }).exec();
    }

    static async edit(_id, id, username, desc) {
        if(_id == id){
            try{
                let user = await User.findById({ _id }).exec();
                user.username = username;
                user.desc = desc;
                user.save();
                return user;
            }catch(err){
                console.log(err);
            }
            let user = await User.findById({ _id }).exec();
            user.username = username;
            user.desc = desc;
            user.save();
            return user;
        }else{
            throw 'not authorised'
        }
    }

    static async follow(_id, followerId) {
        let me = await User.findById(_id).exec();
        let follower = await User.findById(followerId).exec();

        me.following.push(objectId(followerId));

        console.log("here");

        try{
            follower.followers.ids.push(objectId(_id));
            follower.followers.number += 1;
        }catch(err){
            console.log(err);
            follower.followers = { "ids": [objectId(_id)], "number": 1}
        }

        console.log("now here");

        me.save();
        follower.save();

        return true;
    }

    static async unfollow(myId, followerId) {
        let me, follower;
        try{
            me = await User.findById({ "_id" : myId}).exec();
        }catch(err){
            console.log(err);
        }

        try{
            follower = await User.findById({ "_id" : followerId}).exec();
        }catch(err){
            console.log(err);
        }

        me.following.splice(me.following.indexOf(followerId,0) ,1);

        console.log(follower);

        follower.followers.ids.splice(follower.followers.ids.indexOf(myId,0) ,1);
        follower.followers.number -= 1;
        
        console.log(follower);

        me.save();
        follower.save();

        return true;
    }
}

module.exports = UserService;