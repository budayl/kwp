const express = require('express');
const bodyParser = require('body-parser');
const path = require('path');
const morgan = require('morgan')
var multer  = require('multer')
//var upload = multer({ dest: '../client/assets/img/' });
var img = 1;
var currCover = 0;
var currProfile = 0;
var storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, '../client/assets/img/')
    },
    filename: function (req, file, cb) {
        let ext = file.originalname.substring(file.originalname.lastIndexOf('.'), file.originalname.length);
        cb(null, img + ext)
    }
});
const upload = multer({
    storage: storage
}).any();

/*
var storage = multer.diskStorage({
    destination: function(req, file, callback){
        callback(null, './public/uploads'); // set the destination
    },
    filename: function(req, file, callback){
        callback(null, Date.now() + '.jpg'); // set the file name and extension
    }
});
var upload = multer({storage: storage});*/

let app = express();
app.use(morgan('tiny'));
app.use(bodyParser.urlencoded({ extended: true }));
app.use(express.static(path.join(__dirname, '../client')));



class User {
    constructor(id, username, email, password) {
        this.id = id;
        this.username = username;
        this.email = email;
        this.password = password;
    }
}

let users = [
    new User(1, 'aaa', 'aaa@aaa.com', 'aaa'),
    new User(2, 'bbb', 'bbb@bbb.com', 'bbb')
];

let messages = ['samojedna'];

var coverImage = "cover-image.jpg";

app.post('/addCover', (req, res, next) => {
    upload(req, res, function (err) {
      if (err) {
        console.log(err);
        res.sendFile(path.join(__dirname, '../client/profil.html'));
  
        return
      }
      img += 1;

      currCover = img-1;
      res.sendFile(path.join(__dirname, '../client/profil.html'));
    })
});

app.post('/addProfile', (req, res, next) => {
    upload(req, res, function (err) {
      if (err) {
        console.log(err);
        res.sendFile(path.join(__dirname, '../client/profil.html'));
  
        return
      }
      img += 1;

      currProfile = img-1;
      res.sendFile(path.join(__dirname, '../client/profil.html'));
    })
});

/*
app.post('/addCover', upload.single('imagename'), function(req, res, next) {
    var image = req.file;
    console.log(image);
    /*
    image.mv("../client/assets/img/"+name, function(err){
        if(err){
            console.log(err);
            res.send("error occured");
        }else{
            res.send("success");
        }
    })
});
*/

app.get('/message', (req, res) => {
    let tosend = [];
    if(messages.length<6){
        tosend = messages;
    }else{ 
        for(let i=0; i<6; i++){
            tosend[i] = messages[i];
        }
    }

    res.json({ tosend });
});

app.get('/allmessage', (req, res) => {
    res.json({ messages });
});

app.get('/images', (req, res) => {
    res.json([currCover, currProfile]);
});


app.post('/login', (req, res) => {
    let username = req.body.username;
    let email = req.body.email;
    let password = req.body.password;

    let user = new User(100, 'N', 'N', 'N');

    for(let i = 0; i < users.length; ++i)
        if(username == users[i].username){
            if(email == users[i].email){
                if(password == users[i].password){
                    user = users[i];
                }
                break;
            }
            break;
        }
    
    if(user.username != 'N'){
        res.sendFile(path.join(__dirname, '../client/profil.html'));
    }else{
        res.json("nista");
    }
    
});


app.post('/newmessage', (req, res) => {
    let message = req.body.message;

    messages.unshift(message);

    let tosend = [];
    if(messages.length<6){
        tosend = messages;
    }else{ 
        for(let i=0; i<6; i++){
            tosend[i] = messages[i];
        }
    }

    res.json({ tosend });
});

app.use('**', (req, res) => {
    res.sendFile(path.join(__dirname, '../client/prijava.html'));
});


app.listen(3000, () => {
    console.log('Listening on port ' + 3000 + '!')
});