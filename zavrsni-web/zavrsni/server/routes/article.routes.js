const express=require('express');

let router=express.Router();
const authMiddleware=require('../utils/jwt').authMiddleware;

const ArticleController=require('../controllers/article.controller');

router.route('')
    .post(authMiddleware, ArticleController.create);

router.route('/feed')
    .get(authMiddleware, ArticleController.getFeed);

router.route('/:id')
    .get(ArticleController.findById);

router.route('/user/:id')
    .get(authMiddleware, ArticleController.findByUserId)

router.route('/edit/:id')
    .post(authMiddleware, ArticleController.edit)

router.route('/comment/:id')
    .post(authMiddleware, ArticleController.comment)

module.exports=router;